STEPS TO RUN THE PROJECT

1. Backend
   -->pull the project repo in local machine
   -->Open the sts and import the project rto_server
   -->let maven install the reqired dependencies and wait till 100% progress completion
   -->create the the database called rto_management in mysql
   -->update the mysql username password in application.properties file
   -->then run the project as spring Boot App
   -->at the first time run project will automatically crete the all the required tables in rto_management database (if everything goes well)
   -->congrats project server is runing now

2. Frontend
   -->open the rto_Frontend folder from pulled repo in vs code editor
   -->and run the command in terminal of vs code
   -->$npm install (it will install all the aditional packages required for runing the react app)
   -->after successfull installation
   -->$npm start (or yarn start)
   -->it will run the react app on port no. 3000
   -->congrats now both the frontend and backend are running and project too.
