



DROP DATABASE rto_management;

CREATE DATABASE rto_management;

USE rto_management;



CREATE TABLE `users` (
	`user_id` INT  AUTO_INCREMENT,
	`aadhar_no` bigint,
	`name` varchar(100) ,
	`role` varchar(15),
	`dob` DATE,
	`address` varchar(150),
	`gender` varchar(20) ,
	`blood_group` varchar(10) ,
	`mobile_no` bigint(20) ,
	`email` varchar(50) ,
	`password` varchar(50) ,
	`photo` blob ,
	PRIMARY KEY (`user_id`)
);
CREATE TABLE `LL_table` (
	`temp_LL_no` int  AUTO_INCREMENT,
	`user_id` int ,
	`rto` varchar(50) ,
	`issue_date` varchar(50) ,
	`expiry_date` DATE ,
	`L_category` varchar(30) ,
	`payment_no` INT ,
	PRIMARY KEY (`temp_LL_no`)
);

CREATE TABLE `DL_table` (
	`DL_no` INT ,
	`temp_LL_no` int ,
	`dl_issue_date` varchar(50) ,
	`dl_expiry_date` DATE ,
	`payment_no` int ,
	PRIMARY KEY (`DL_no`)
);
CREATE TABLE `vehicle_registration` (
    `registration_id` INT  AUTO_INCREMENT,
	`registration_no` varchar(30) ,
	`owner` varchar(30) ,
	`user_id` INT ,
	`make` varchar(20) ,
	`chassis_no` varchar(30) ,
	`vehicle_class` varchar(30) ,
	`purchase_date` DATE ,
	`fuel_type` varchar(20) ,
	`engine_no` varchar(30) ,
	`engine_capacity` int(30) ,
	`insurance_status` BOOLEAN ,
	`puc_status` BOOLEAN ,
	`hypothecated_to` varchar(30) ,
	`payment_id` INT  default '3',
	`wheels` int ,
	`seat_capacity` int ,
	PRIMARY KEY (`registration_id`)
);
 CREATE TABLE `vehicle_transfer` (
	`transfer_id` INT  AUTO_INCREMENT,
    `transfer_no` varchar(100),
    `user_id` INT ,
	`registration_id` INT(30) ,
	`new_owner` varchar(30) ,
	`new_owner_aadhar` bigint ,
	`new_owner_email` varchar(30) ,
	`new_owner_mobile` bigint ,
	`payment_id` INT default '4',
	PRIMARY KEY (`transfer_id`)
);
CREATE TABLE `permit` (
	`permit_id` INT  AUTO_INCREMENT,
	`permit_no` varchar(30) ,
	`registration_id` INT ,
    `user_id` int  ,
	`from_date` DATE ,
	`to_date` DATE ,
	`from_state` varchar(30) ,
	`to_state` varchar(30) ,
	`payment_id` INT default '5',
	PRIMARY KEY (`permit_id`)
);
CREATE TABLE `puc` (
	`puc_id` INT  AUTO_INCREMENT,
    `puc_no` varchar(100),
	`registration_id` INT ,
    `user_id` int ,
	`from_date` DATE ,
	`to_date` DATE ,
	`co2` FLOAT,
	`hc` FLOAT,
	`payment_id` INT DEFAULT '6',
	PRIMARY KEY (`puc_id`)
);


CREATE TABLE `payment_table` (
	`payment_refno` INT  AUTO_INCREMENT,
	`user_id` INT ,
	`payment_id` int  ,
	`payment_mode` varchar(30),
	
	`amount` int,
	`payment_date` DATE ,
	
	PRIMARY KEY (`payment_refno`)
);


 
 
 ALTER TABLE ll_table ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE;
 
--- ALTER TABLE users MODIFY password varchar(1024);

--- ALTER TABLE ll_table ADD FOREIGN KEY(user_id) REFERENCES users(user_id); 


ALTER TABLE DL_table ADD FOREIGN KEY(user_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE; 
 ALTER TABLE DL_table ADD FOREIGN KEY(temp_LL_id) REFERENCES ll_table(temp_LL_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE vehicle_registration ADD FOREIGN KEY(user_id) REFERENCES users(user_id) ON DELETE CASCADE ON UPDATE CASCADE; 



ALTER TABLE vehicle_transfer ADD FOREIGN KEY(user_id) REFERENCES users(user_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE vehicle_transfer ADD FOREIGN KEY(registration_id) REFERENCES vehicle_registration(registration_id) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE permit ADD FOREIGN KEY(user_id) REFERENCES users(user_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE permit ADD FOREIGN KEY(registration_id) REFERENCES vehicle_registration(registration_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE puc ADD FOREIGN KEY(user_id) REFERENCES users(user_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE puc ADD FOREIGN KEY(registration_id) REFERENCES vehicle_registration(registration_id) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE payment_table ADD FOREIGN KEY(user_id) REFERENCES users(user_id) ON DELETE CASCADE ON UPDATE CASCADE;




INSERT INTO users(user_id,aadhar_no,name,role,dob,address,gender,blood_group,mobile_no,email,password) VALUES(1,1234567890,'shubham jadhav','admin','1997-12-11','karad','male','a',7558379411,'shubham@test.com','shubh');

  insert into ll_table (temp_LL_no , user_id , rto , issue_date , expiry_date , L_category) values ('MH98 1234',1, 'karad', '2021-11-12' , '2021-11-12'  , 'LMV');


insert into DL_table(DL_no ,user_id , temp_LL_id , dl_issue_date , dl_expiry_date) values ('MH D 1234',1,1,'2010-12-25', '2022-01-01');

insert into vehicle_registration(registration_no , owner , user_id , make , chassis_no , vehicle_class , purchase_date , fuel_type , engine_no , engine_capacity , insurance_status ,puc_status , hypothecated_to ,  wheels ,seat_capacity) values ('UP93 XY 1234 ', 'Shubham', 1 , 'Maruti' , 'M1235' , 'Car' , '2018-01-12' , 'Diesel' , 'A1234', 720 , 1, 1,'Insurance' , 4, 4);


 insert into vehicle_transfer (transfer_no ,user_id , registration_id , new_owner , new_owner_aadhar , new_owner_email , new_owner_mobile ) values ('MH T 1234', 1,1, 'Suraj ' , 455159223486 , 'shubhanshu@test.com' , 8417875195 );
 


 insert into permit ( permit_no , registration_id , user_id , from_date , to_date , from_state , to_state ) values ('UP123MP', 1,1,'2022-01-05','2022-01-20','UTTAR PRADESH','MADHYA PRADESH');


 insert into puc (puc_no , registration_id , user_id , from_date , to_date , co2 , hc ) values ('MH puc 1234', 1, 1, '2018-02-15','2022-12-10',8.48,40.2);


 insert into payment_table(user_id, payment_id , payment_mode , amount ,payment_date) values (1, 2 , 'cash', 500 , '2022-01-05');







